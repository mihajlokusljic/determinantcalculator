# Računanje determinante matrice primjenom Laplasovog razvoja po prvoj vrsti

# Članovi tima

- Mihajlo Kušljić, SW53-2016

# Video prezentacija

https://drive.google.com/file/d/13VfjKkgoV427h7GxToKhCThv1NIbyLHe/view

# Osnovni pojmovi
- Matrica formata mn nad poljem F je funkcija Mmn koja preslikava skup uređenih parova {(𝑖,𝑗)|𝑖 ∈ {1,2,…,𝑚} ∧ 𝑗 ∈ {1,2,…,𝑛}} u skup F.
- Matrice formata nn, tj. matrice koje imaju isti broj redova i kolona, nazivamo kvadratne matrice reda n.
- Kvadratna podmatrica reda r matrice Mmn je kvadratna matrica reda r koja se dobija kada se iz matrice Mmn izbaci proizvoljnih (m - r) vrsta i (n - r) kolona.
- Determinanta je funkcija koja preslikava skup svih kvadratnih matrica u skup F, gdje je F polje nad kojim su definisane matrice.
- Minor reda r neke matrice Mmn je determinanta neke njene kvadratne podmatrice reda r.
- Jedna od metoda izračunavanja determinante matrice je Laplasov razvoj po vrsti/koloni. Ako razložimo matricu A po vrsi i, tada je: det(𝐴)= Σ[ (−1)^(i + j) * Aij * Mij ], gdje je Mij minor, tj. determinanta matrice koja se dobija kada se iz matrice A ukloni i-ta vrsta i j-ta kolona.

# Motivacija problema
Determinante imaju široku primjenu u mnogim oblastima matematike. U algebri determinante se koriste za opisivanje invertibilnih matrica (imaju inverznu matricu) i da se opiše rješenje sistema linearnih jednačina pomoću Kramerovog pravila. Determinante se koriste i da se izračunaju zapremine u vektorskoj analizi: apsolutna vrijednost determinante realnih vektora jednaka je zapremini paralelopipeda koji grade ti vektori. Takođe na osnovu determinante može se utvrditi linearna zavisnost realnih vektora. U matematičkoj analizi, pri računanju integrala po podskupovima Euklidskog prostora R^n, koriste se tzv. jakobijani koji predstavljaju determinante Jakobijevih matrica...

# Opis problema
Upotrebom programskih jezika Python i Go obezbjediti serijsku i paralelnu implementaciju računanja determinante kvadratne matrice reda n, primjenom Laplasovog razvoja po prvoj vrsti. Matrica se učitava iz teksutalnog fajla koji prati slijedeći format:

- Prvi red u fajlu sadrži red matrice (n)
- Narednih n redova u fajlu sadrže vrste matrice, gdje su elemeni razdvojeni barem jednim razmakom

Po učitavanju matrice izvršava se serijski i paralelni proračun determinante matrice. Rezultati se dodaju u CSV fajl koji ima slijedeće zaglavlje:

- n - red matrice čija je determinanta izračunata
- exec_time_ms - vrijeme izvršavanja proračuna u milisekundama
- serial - da li je proračun serijski, moguće vrijednosti su: true (serijski propračun), false (paralelizovan proračun)
- implementation - jezik u kojem je propračun implementiran, moguće vrijednosti su: python, go

Argumenti programa su putanje do (validnih) datoteka koje sadrže matrice čije determinante treba izračunati i putanja do CSV fajla u kojem se čuvaju rezultati. Pomoću programskog jezika Pharo Smalltalk omogućiti učitavanje generisanih rezultata iz CSV fajla i poređenje performansi implementacija u Python i Go jeziku na grafikonu (prikaz vremena izvršavanja u odnosu na red matrice).

# Koncept rješenja
Determinanta matrice računa se rekurzivnom primjenom Laplasovog razvoja po prvoj vrsti. Za računanje determinante matrice reda n razvojem po vrsti 0 potrebno je najprije odrediti vrijednosti n njenih minora: M[0,0], M[0,1], ... M[0,n-1]. Računanje svakog od ovih minora svodi se na određivanje determinante podmatrice reda n – 1, ponovo primjenom Laplasovog razvoja po prvoj vrsti podmatrice... Baza rekurzije je slučaj kada se dođe do podmatrice reda 1. Tada je vrijednost determinante jednaka vrijednosti jedinog elementa podmatrice. Kada se završi računanje pomenutih minora, vrijednost determinante se određuje po formuli: det(𝐴)= Σ[ (−1)^(0 + j) * A0j * M0j ]. Možemo primjetiti da u sumi prvi činilac naizmjenično uzima vrijednosti 1 i -1, drugi činilac je element polazne matrice a treći činilac je jedan od izračunatih minora.
Proračuni minora M[0,0], M[0,1], ... M[0,n-1], potrebnih za određivanje determinante matrice, su međusobno nezavisni i mogu se izvršiti paralelno. Za određivanje determinante matrice reda n koristeći p niti/procesnih jedinica, zadaci određivanja minora M[0,0], M[0,1], ... M[0,n-1], dodjeljuju se procesnim jedinicama po Round-Robin principu. Npr. za matricu reda 5 i 3 procesne jedinice (u oznaci p0, p1, p2) računanje M[0,0] dodjeljuje se procesnoj jedinici p0, računanje M[0,1] dodjeljuje se p1, računanje M[0,2] dodjeljuje se p2, računanje M[0,3] dodjeljuje se p0 i računanje M[0,4] dodjeljuje se p1. Procesne jedinice izračunavaju u paraleli dodjeljene minore. Kada se izračunaju minori, početna procesna jedinica, p0, određuje vrijednost determinante. Svaki od pomenutih minora je reda n-1. U idealnom slučaju, kada je red matrice djeljiv brojem procesnih jedinica, sve procesne jedinice računaju isti broj minora istih dimenzija, pa svaka procesna jedinica dobija istu količinu posla. Ukoliko red matrice nije djeljiv brojem procesnih jedinica, neke procesne jedinice će imati više posla od drugih, što dovodi do neefikasnog korišćenja procesnih jedinica i ograničava ubrzanje. Npr., u navedenom primjeru procesne jedinice p0 i p1 treba da izračunaju  2 minora, dok procesna jedinica p2 treba da izračuna jedan minor. U opštem slučaju za određivanje determinante matrice reda n u paralelnoj implementaciji koristi se n procesnih jedinica. Tada svaka procesna jedinica treba da izračuna jedan minor, reda n – 1, čime se svim procesnim jedinicama dodjeljuje jednaka količina posla.